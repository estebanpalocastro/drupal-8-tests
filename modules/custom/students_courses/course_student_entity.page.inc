<?php

/**
 * @file
 * Contains course_student_entity.page.inc.
 *
 * Page callback for name entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for name templates.
 *
 * Default template: course_student_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_course_student_entity(array &$variables) {
  // Fetch CourseStudentEntity Entity Object.
  $course_student_entity = $variables['elements']['#course_student_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
