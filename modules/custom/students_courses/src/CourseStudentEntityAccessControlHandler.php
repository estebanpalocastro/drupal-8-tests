<?php

namespace Drupal\students_courses;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the name entity.
 *
 * @see \Drupal\students_courses\Entity\CourseStudentEntity.
 */
class CourseStudentEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\students_courses\Entity\CourseStudentEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished name entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published name entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit name entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete name entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add name entities');
  }

}
