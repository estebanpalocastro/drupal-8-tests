<?php

namespace Drupal\students_courses\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CourseStudentEntityTypeForm.
 */
class CourseStudentEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $course_student_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $course_student_entity_type->label(),
      '#description' => $this->t("Label for the name type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $course_student_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\students_courses\Entity\CourseStudentEntityType::load',
      ],
      '#disabled' => !$course_student_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $course_student_entity_type = $this->entity;
    $status = $course_student_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label name type.', [
          '%label' => $course_student_entity_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label name type.', [
          '%label' => $course_student_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($course_student_entity_type->toUrl('collection'));
  }

}
