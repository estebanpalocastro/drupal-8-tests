<?php

namespace Drupal\students_courses\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\students_courses\CourseStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Form controller for name edit forms.
 *
 * @ingroup students_courses
 */
class CourseStudentEntityForm extends ContentEntityForm {
  
  
  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * Massenger manager service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  
  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;
  
  protected $courseStorage;
  
  public function __construct(
    EntityManagerInterface $entity_manager,
    EntityTypeManagerInterface $entity_type_manager,
    CourseStorage $course_storage,
    MessengerInterface $messenger,
    CurrentRouteMatch $route_match
    
  ) {
    parent::__construct($entity_manager);
    $this->entityTypeManager = $entity_type_manager;
    $this->courseStorage = $course_storage;
    $this->messenger = $messenger;
    $this->routeMatch = $route_match;
    
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('entity_type.manager'),
      $container->get('students_courses.storage'),
      $container->get('messenger'),
      $container->get('current_route_match')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\students_courses\Entity\CourseStudentEntity */
    $form = parent::buildForm($form, $form_state);
    
    $element = $form_state->getTriggeringElement();
    $topics = ['_none' => $this->t('- Select class topic -')];
    $timeslot_values = [];
    // Assing the field names.
    $fields = [
      'field_class_subject',
      'field_class_topic',
    ];
  
    // Identify if new or old entity default content.
    if (!$this->entity->isNew() && empty($element)) {
      
      $temp['field_class_subject'] = $this->entity->field_class_subject->getValue();
      $temp['field_class_topic'] = $this->entity->field_class_topic->getValue();
      $temp['field_timeslot'] = $this->entity->field_timeslot->getValue();
    }
    
    // Identify element call ajax for assign value to variable temp.
    if (!empty($element) && isset($element['#field_name']) && in_array($element['#field_name'], $fields)) {
      
      $temp['field_class_subject'] = $form_state->getValue('field_class_subject');
      $temp['field_class_topic'] = $form_state->getValue('field_class_topic');
      $form_state->set('temp', $temp);
      
    }
  
    if (!empty($temp['field_class_subject'])) {
      $timeslot_values = ['_none' => $this->t('- Select classes timeslot -')];
      $properties = [
        'vid' => 'class_topic',
        'field_subject' => $temp['field_class_subject'][0]['target_id'],
      ];
      $all_class_topic = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties($properties);
    
      foreach ($all_class_topic as $topic) {
        $tid = $topic->tid->first()->getValue();
        $name = $topic->name->first()->getValue();
        $topics[$tid['value']] = $name['value'];
      }
    }
  
    if (!empty($temp['field_class_topic'])) {
      $topic = $this->entityTypeManager->getStorage('taxonomy_term')->load($temp['field_class_topic'][0]['target_id']);
    
      if ($topic) {
        $timeslots = $topic->get('field_timeslot')->referencedEntities();
        foreach ($timeslots as $timeslot) {
          $tid = $timeslot->id();
          $name = $timeslot->getName();
          $timeslot_values[$tid] = $name;
        }
      }
    }
    
    $form['#tree'] = TRUE;
    $form['field_class_subject']['#prefix'] = '<div id="template-type-wrapper">';
    $form['field_class_subject']['#suffix'] = '</div>';
    $form['field_class_subject']['widget']['#ajax'] = [
      'callback' => '::ajaxClassTopic',
      'wrapper' => 'creativity-location-wrapper',
    ];
    
    $form['field_class_topic']['#prefix'] = '<div id="creativity-location-wrapper">';
    $form['field_class_topic']['#suffix'] = '</div>';
  
    if (empty($form['field_class_topic']['widget'])) {
      return $form;
    }
    
    $form['field_class_topic']['widget']['#ajax'] = [
      'callback' => '::ajaxTimeslotsCallback',
      'wrapper' => 'creativity-topic-wrapper',
    ];
  
    $form['field_timeslot']['#prefix'] = '<div id="creativity-topic-wrapper">';
    $form['field_timeslot']['#suffix'] = '</div>';
  
    if (empty($form['field_class_topic']['widget'])) {
      return $form;
    }
    
    if (!empty($this->routeMatch->getParameter('course_student_entity')) && empty($element) && !isset($element['#field_name'])) {
      $form['field_class_subject']['widget']['#options']['_none'] = $this->t('- Select a value -');
      $form['field_class_subject']['widget']['#default_value'] = ['_none'];
      $topics = ['_none' => $this->t('- Select class topic -')];
      $timeslot_values = ['_none' => $this->t('')];
    
      $header = $this->overviewFormHeader();
      $student_id = $this->entity->get('field_student_id')->getValue();
      $rows = [];
      $courses = $this->courseStorage->getCoursesByStudent($header, $student_id[0]['value']);
      foreach ($courses as $course) {
        $topic = $this->entityTypeManager->getStorage('taxonomy_term')->load($course->id_topic);
        $subject = $topic->get('field_subject')->entity;
        $timestlot = $this->entityTypeManager->getStorage('taxonomy_term')->load($course->id_timestlot);
        $rows[] = [
          'subject' => $subject->getName(),
          'topic' => $topic->getName(),
          'timeslot' => $timestlot->getName(),
        ];
      }
      
      $form['courses'] = [
        '#header' => $header,
        '#type' => 'table',
        '#rows' => $rows,
        '#weight' => 10,
      ];
      
    }
    $form['field_timeslot']['widget']['#options'] = $timeslot_values;
    $form['field_class_topic']['widget']['#options'] = $topics;
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $form_state->cleanValues();
    $values = $form_state->getValues();
    \Drupal::request()->query->remove('destination');
    \Drupal::destination()->set(NULL);
    $response = $this->courseStorage->checkCourseByStudent($values);
    if (!$response && $entity->isNew()) {
      
      $status = $this->entity->save();
      
      switch ($status) {
        case SAVED_NEW:
          drupal_set_message($this->t('Created the %label name.', [
            '%label' => $entity->label(),
          ]));
          break;
    
        default:
          drupal_set_message($this->t('Saved the %label name.', [
            '%label' => $entity->label(),
          ]));
      }
      $form_state->setRedirect('entity.course_student_entity.edit_form', ['course_student_entity' => $entity->id()]);
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $form_state->cleanValues();
    $values = $form_state->getValues();
    // Validate whe request is not ajax;
    if ($form_state->isSubmitted()) {
      $properties = [
        'field_student_id' => $form_state->getValue('field_student_id')[0]['value'],
      ];
      $student = $this->entityTypeManager->getStorage('course_student_entity')->loadByProperties($properties);
      
      if (!empty($student)) {
        $response = $this->courseStorage->checkCourseByStudent($values);
        if (!empty($response)) {
          $form_state->setErrorByName('course_student_entity', $this->t('The course of student is already register in the system.'));
        }
        else {
          $this->courseStorage->insertStudentCourse($values);
          $this->messenger->addMessage('The course of student has been create successful.');
          $key = array_keys($student);
          $form_state->setRedirect('entity.course_student_entity.edit_form', ['course_student_entity' => $student[$key[0]]->id()]);
        }
      }
    }
    
    
  }
  
  /**
   * Submit form ajax for topics.
   *
   * @param array $form
   *   The form elements array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The modified form array.
   */
  public function ajaxClassTopic(array &$form, FormStateInterface $form_state) {
    return $form['field_class_topic'];
  }
  
  /**
   * Submit form ajax for timeslots.
   *
   * @param array $form
   *   The form elements array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The modified form array.
   */
  public function ajaxTimeslotsCallback(array &$form, FormStateInterface $form_state) {
    return $form['field_timeslot'];
  }
  
  /**
   * Gets overview form header.
   *
   * @return array
   *   Header array definition as expected by theme_tablesort().
   */
  public function overviewFormHeader() {
    $header = [
      'subject' => ['data' => $this->t('Class subject')],
      'topic' => ['data' => $this->t('Class topic')],
      'timeslot' => ['data' => $this->t('Class timeslot')],
    ];
    
    return $header;
  }

}
