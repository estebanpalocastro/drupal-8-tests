<?php

namespace Drupal\students_courses\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting name entities.
 *
 * @ingroup students_courses
 */
class CourseStudentEntityDeleteForm extends ContentEntityDeleteForm {


}
