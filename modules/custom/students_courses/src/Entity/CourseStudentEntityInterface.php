<?php

namespace Drupal\students_courses\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining name entities.
 *
 * @ingroup students_courses
 */
interface CourseStudentEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the name name.
   *
   * @return string
   *   Name of the name.
   */
  public function getName();

  /**
   * Sets the name name.
   *
   * @param string $name
   *   The name name.
   *
   * @return \Drupal\students_courses\Entity\CourseStudentEntityInterface
   *   The called name entity.
   */
  public function setName($name);

  /**
   * Gets the name creation timestamp.
   *
   * @return int
   *   Creation timestamp of the name.
   */
  public function getCreatedTime();

  /**
   * Sets the name creation timestamp.
   *
   * @param int $timestamp
   *   The name creation timestamp.
   *
   * @return \Drupal\students_courses\Entity\CourseStudentEntityInterface
   *   The called name entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the name published status indicator.
   *
   * Unpublished name are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the name is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a name.
   *
   * @param bool $published
   *   TRUE to set this name to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\students_courses\Entity\CourseStudentEntityInterface
   *   The called name entity.
   */
  public function setPublished($published);

}
