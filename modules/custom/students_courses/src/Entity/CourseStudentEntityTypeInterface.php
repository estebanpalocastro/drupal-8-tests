<?php

namespace Drupal\students_courses\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining name type entities.
 */
interface CourseStudentEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
