<?php

namespace Drupal\students_courses\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the name type entity.
 *
 * @ConfigEntityType(
 *   id = "course_student_entity_type",
 *   label = @Translation("name type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\students_courses\CourseStudentEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\students_courses\Form\CourseStudentEntityTypeForm",
 *       "edit" = "Drupal\students_courses\Form\CourseStudentEntityTypeForm",
 *       "delete" = "Drupal\students_courses\Form\CourseStudentEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\students_courses\CourseStudentEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "course_student_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "course_student_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/course_student_entity_type/{course_student_entity_type}",
 *     "add-form" = "/admin/structure/course_student_entity_type/add",
 *     "edit-form" = "/admin/structure/course_student_entity_type/{course_student_entity_type}/edit",
 *     "delete-form" = "/admin/structure/course_student_entity_type/{course_student_entity_type}/delete",
 *     "collection" = "/admin/structure/course_student_entity_type"
 *   }
 * )
 */
class CourseStudentEntityType extends ConfigEntityBundleBase implements CourseStudentEntityTypeInterface {

  /**
   * The name type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The name type label.
   *
   * @var string
   */
  protected $label;

}
