<?php

namespace Drupal\students_courses;

use Drupal\Core\Database\Connection;

/**
 * Class CourseStorage.
 */
class CourseStorage {
  
  /**
   * Drupal\Core\Database\database definition.
   *
   * @var \Drupal\Core\Database\database
   */
  protected $database;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }
  
  
  /**
   * Function to insert relationship course with student.
   *
   * @param $values
   * @return bool|\Drupal\Core\Database\StatementInterface|int|string|null
   */
  public function insertStudentCourse($values) {
    $id_student = $values['field_student_id'][0]['value'];
    $timeslots_values = $values['field_timeslot'];
    $id_class_topic = $values['field_class_topic'][0]['target_id'];
    $id_class_subject = $values['field_class_subject'][0]['target_id'];
    
    try {
      $courses = [];
      $query = $this->database->insert('students_courses');
      $query->fields(['id_student', 'id_subject', 'id_timestlot', 'id_topic']);
      foreach ($timeslots_values as $timeslot) {
        $courses[] = [
          'id_student' => $id_student,
          'id_subject' => $id_class_subject,
          'id_timestlot' => $timeslot['target_id'],
          'id_topic' => $id_class_topic
        ];
      }
      foreach ($courses as $course) {
        $query->values($course);
      }
      $result = $query->execute();
    } catch (\Exception $e) {
      $result = $e->getCode() == 23000 ? 'repeat' : FALSE;
    }
    
    return $result;
  }
  
  /**
   * Function to verify if student register course.
   *
   * @param $values
   * @return mixed
   */
  public function checkCourseByStudent($values) {
    $id_student = $values['field_student_id'][0]['value'];
    $timeslots_values = array_column($values['field_timeslot'], 'target_id');
    $id_class_topic = $values['field_class_topic'][0]['target_id'];
    $query = $this->database->select('students_courses');
    $query->fields('students_courses', ['id', 'id_timestlot', 'id_topic']);
    $query->condition('id_student', $id_student);
    $query->condition('id_timestlot', $timeslots_values, 'IN');
    $query->condition('id_topic', $id_class_topic);
    return $query->execute()->fetchAll();
    
  }
  
  /**
   * Function to get courses by student registerd.
   *
   * @param $header
   * @param $student_id
   * @param int $num_rows
   * @return mixed
   */
  public function getCoursesByStudent($header, $student_id, $num_rows = 30) {
    $db = $this->database;
    $query = $db->select('students_courses', 'sc');
    $query->fields('sc', [
      'id_subject',
      'id_topic',
      'id_timestlot'
    ]);
    $query->condition('sc.id_student', $student_id);
    $query->orderBy('id_subject', 'id_topic');
    
    return $query->execute()->fetchAll();
  }
  
}