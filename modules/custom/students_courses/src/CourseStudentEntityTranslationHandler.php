<?php

namespace Drupal\students_courses;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for course_student_entity.
 */
class CourseStudentEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
